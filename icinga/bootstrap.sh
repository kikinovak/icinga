#!/bin/bash
#
# bootstrap.sh
echo "Configuring firewall."
firewall-cmd --permanent --zone=external --change-interface=eth0 > /dev/null
firewall-cmd --permanent --zone=internal --change-interface=eth1 > /dev/null
firewall-cmd --set-default-zone=internal > /dev/null
firewall-cmd --permanent --remove-service=dhcpv6-client > /dev/null
firewall-cmd --permanent --remove-service=mdns > /dev/null
firewall-cmd --permanent --remove-service=samba-client > /dev/null
firewall-cmd --permanent --add-service=ntp > /dev/null
firewall-cmd --permanent --add-service=dhcp > /dev/null
firewall-cmd --permanent --add-service=dns > /dev/null
firewall-cmd --reload > /dev/null
echo "Configuring NTP server."
{ echo '# /etc/chrony.conf'
  echo 'server 0.fr.pool.ntp.org iburst'
  echo 'server 1.fr.pool.ntp.org iburst'
  echo 'server 2.fr.pool.ntp.org iburst'
  echo 'server 3.fr.pool.ntp.org iburst'
  echo 'driftfile /var/lib/chrony/drift'
  echo 'makestep 1.0 3'
  echo 'rtcsync'
  echo 'logdir /var/log/chrony'
} > /etc/chrony.conf
systemctl restart chronyd > /dev/null
echo "Configuring local hosts."
{ echo '# /etc/hosts'
  echo '127.0.0.1   localhost.localdomain localhost'
  echo '10.23.45.10 icinga.sandbox.lan icinga'
  echo '10.23.45.20 server-01.sandbox.lan server-01'
  echo '10.23.45.30 server-02.sandbox.lan server-02'
} > /etc/hosts
echo "Configuring DNS server."
yum install -y dnsmasq > /dev/null
{ echo '# /etc/dnsmasq.conf'
  echo 'domain-needed'
  echo 'bogus-priv'
  echo 'interface=eth1'
  echo 'dhcp-range=10.23.45.100,10.23.45.200,24h'
  echo 'local=/sandbox.lan/'
  echo 'domain=sandbox.lan'
  echo 'expand-hosts'
  echo 'server=1.1.1.1'
  echo 'server=1.0.0.1'
  echo 'no-resolv'
  echo 'log-facility=/var/log/dnsmasq.log'
} > /etc/dnsmasq.conf
systemctl enable dnsmasq --now > /dev/null 2>&1
{ echo '# /etc/resolv.conf'
  echo 'nameserver 127.0.0.1'
} > /etc/resolv.conf
echo "Adding Icinga repository."
sudo yum install -y \
  https://packages.icinga.com/epel/icinga-rpm-release-7-latest.noarch.rpm \
  > /dev/null
echo "Installing Icinga."
yum install -y icinga2 > /dev/null 2>&1
echo "Activating Icinga."
systemctl enable icinga2 --now > /dev/null 2>&1
echo "Installing essential Nagios plugins."
yum install -y nagios-plugins-{ping,ssh,http,disk,load,procs,swap,users} > /dev/null
echo "Installing SELinux policy for Icinga."
yum install -y icinga2-selinux > /dev/null
echo "Enabling Vim syntax highlighting for Icinga."
yum install -y vim-icinga2 > /dev/null
echo "Installing MySQL database server."
yum install -y mariadb-server > /dev/null
echo "Activating MySQL database server."
systemctl enable mariadb --now > /dev/null 2>&1
echo "Installing IDO modules for MySQL."
yum install -y icinga2-ido-mysql > /dev/null
echo "Creating database for Icinga."
mysqladmin create icinga
mysql -e "GRANT ALL ON icinga.* TO icinga@localhost \
          IDENTIFIED BY 'icinga123';"
mysql -e "FLUSH PRIVILEGES;"
echo "Importing Icinga IDO schema."
mysql -u root icinga < /usr/share/icinga2-ido-mysql/schema/mysql.sql
echo "Creating database for Icinga Web."
mysqladmin create icingaweb
mysql -e "GRANT ALL ON icingaweb.* TO icingaweb@localhost \
          IDENTIFIED BY 'icingaweb123';"
mysql -e "FLUSH PRIVILEGES;"
echo "Securing MySQL database server."
echo -e "\n\nroot123\nroot123\n\n\n\n\n" | mysql_secure_installation \
  > /dev/null 2>&1
echo "Updating database credentials."
{ echo '// /etc/icinga2/features-available/ido-mysql.conf'
  echo
  echo 'object IdoMysqlConnection "ido-mysql" {'
  echo '  user = "icinga"'
  echo '  password = "icinga123"'
  echo '  host = "localhost"'
  echo '  database = "icinga"'
  echo '}'
} > /etc/icinga2/features-available/ido-mysql.conf
echo "Enabling IDO MySQL feature."
icinga2 feature enable ido-mysql > /dev/null
systemctl restart icinga2
echo "Installing Apache web server."
yum install -y httpd > /dev/null
echo "Activating Apache web server."
systemctl enable httpd --now > /dev/null 2>&1
echo "Enabling port 80."
firewall-cmd --permanent --add-service=http > /dev/null
firewall-cmd --reload > /dev/null
echo "Enabling API feature."
icinga2 api setup > /dev/null
{ echo '// /etc/icinga2/conf.d/api-users.conf'
  echo
  echo 'object ApiUser "icingaweb2" {'
  echo '  password = "api123"'
  echo '  permissions = [ "status/query", "actions/*", "objects/modify/*",'
  echo '  "objects/query/*" ]'
  echo '}'
} > /etc/icinga2/conf.d/api-users.conf
systemctl restart icinga2
echo "Installing Icinga Web."
yum install -y icingaweb2 icingacli > /dev/null 2>&1
echo "Installing PHP Imagick module."
yum install -y sclo-php73-php-pecl-imagick > /dev/null
echo "Installing SELinux policy for Icinga Web."
yum install -y icingaweb2-selinux > /dev/null
echo "Setting PHP timezone."
{ echo '; /etc/opt/rh/rh-php73/php.d/20-date.ini'
  echo '[Date]'
  echo '; Defines the default timezone used by the date functions'
  echo '; http://php.net/date.timezone'
  echo 'date.timezone = Europe/Paris'
} > /etc/opt/rh/rh-php73/php.d/20-date.ini
echo "Activating PHP-FPM."
systemctl enable rh-php73-php-fpm --now > /dev/null 2>&1
systemctl restart httpd
echo "Preparing Icinga for clients:"
echo "  - Master/satellite setup."
echo "  - Do not exclude conf.d directory."
echo -e "n\n\n\n\n\n\nn\n" | icinga2 node wizard > /dev/null 2>&1
echo "Backing up initial configuration."
cp -R /etc/icinga2/conf.d /etc/icinga2/conf.d.orig
echo "Importing initial configuration."
cp -R /vagrant/conf.d/* /etc/icinga2/conf.d/
systemctl restart icinga2 > /dev/null 2>&1
echo "Generating setup token."
icingacli setup token create
echo "Open http://10.23.45.10/icingaweb2 in a web browser."
echo "Authenticate using the setup token."
exit 0

